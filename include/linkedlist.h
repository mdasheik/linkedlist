#ifndef LINKEDLIST_H
#define LINKEDLIST_H

/* Single self referential node and list definition */
typedef struct node
{
    int data;
    struct node *pNextNode;
}node;

typedef struct slist
{
   node *pHeadNode;
   node *pTailNode;
   int nodeCount;
}slist;

/* Function prototypes */
slist *InitSlit(slist *pslist);
slist *AddNodeToHead(node *pNode,slist *pslist, int data);
node *reverseList(node *pLast, node *pNode);
node *recursiveReverse(node *pNode);
void DisplaySList(slist *pslist);
/* End of fucntion prototypes */

#endif
