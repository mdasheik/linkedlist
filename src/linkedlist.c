#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/linkedlist.h"


/*
Initialze the slist and its pointers to NULL */
slist *InitSlist(slist *pslist)
{
    pslist = (slist *)malloc(sizeof(slist));
    pslist->pHeadNode = NULL;
    pslist->pTailNode = NULL;
    pslist->nodeCount = 0;
    return pslist;
}

/* add node at beginning */
node *push(node *pNode , int data)
{
    node *pNewNode = (node *)malloc(sizeof(node));
    pNewNode->pNextNode = pNode;
    pNewNode->data = data;
    pNode = pNewNode;
    return pNode;
}

/* remove a node from top */
node *pop(node *pNode,int data)
{
    node *ptemp = pNode;
    pNode = pNode->pNextNode;
    free(ptemp);
    return pNode;
}

node *reverseList(node *pLast, node *pNode)
{
    if (pNode == NULL)
       return pLast;
    node *pNextNode = pNode->pNextNode;
    pNode->pNextNode = pLast;
    return reverseList(pNode,pNextNode);
}
/* Recursively reverse list */
node *recursiveReverse(node *pNode)
{
    return reverseList(NULL, pNode);
}


/* Add node at beginning and update the pointers of slist */
slist *AddNodeToHead(node *pNode,slist *pslist,int data)
{
    if(pNode != NULL)
    {
        pNode = push(pNode,data);
        pslist->pHeadNode = pNode;
        pslist->nodeCount += 1;
    }
    else
    {
        printf("Empty node has been passed , adding first node\n") ;
        pNode = push(pNode, data);
        pslist->pHeadNode = pNode;
        pslist->nodeCount += 1;
    }
    return pslist;
}

/* Display the list */
void DisplaySList(slist *pslist)
{
    if(pslist->pHeadNode != NULL)
    {
       node *pcurrentNode = pslist->pHeadNode;
       while(pcurrentNode->pNextNode != NULL)
       {
           printf("%d,", pcurrentNode->data);
           pcurrentNode = pcurrentNode->pNextNode;
       }
       printf("%d\n", pcurrentNode->data);
    }
    else
        printf("Empty node has been passed,so returning ");

}
